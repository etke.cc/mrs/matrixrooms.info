# MatrixRooms.info website [![Donate on Liberapay](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/mrs/donate)

This repository contains the frontend website of the [MatrixRooms.info](https://matrixrooms.info) service (powered by the [mrs/api](https://gitlab.com/etke.cc/mrs/api) backend).


## Running

To run in **development** mode:

- run `just run`
- open https://localhost:3000 in a browser

To run in **production** mode:

- run `just build` to build static files in `dist/`
- run `just serve` to serve these static files
- open https://localhost:3000 in a browser

### Fixing CORS issues

The website is configured to talk to the API at `https://apicdn.matrixrooms.info`. Due to **CORS restrictions**, you may not be able get this localhost-hosted website to consume this API.

To work around this problem, you may install a browser addon like [Allow CORS: Access-Control-Allow-Origin](https://mybrowseraddon.com/access-control-allow-origin.html) for [Chromium-based-browsers](https://chrome.google.com/webstore/detail/allow-cors-access-control/lhobafahddgcelffkeicbaginigeejlf) or for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/access-control-allow-origin/).

Instructions for using this addon can be found on [this YouTube video](https://www.youtube.com/watch?v=KruSUqLdxQA) (TLDW: there's an **ON** toggle button that needs to be clicked).
