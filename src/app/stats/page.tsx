import TimeLineStats from '@/components/TimeLineStats';
import Link from 'next/link';
import { Suspense } from 'react';

function TimeLineStatsFallback() {
	return <>Loading Matrix Rooms and Servers data...</>
}

export default function StatsPage() {
	return (<div className="p-2 my-4 w-100 mx-auto">
		<Suspense fallback={<TimeLineStatsFallback />}>
			<TimeLineStats />
		</Suspense>
		<div className="mt-6 mx-2">
			<h3 className="text-2xl sm:text-4xl dark:text-white leading-normal text-center">
				Want to host your <span className="font-bold">own</span> Matrix server?
				<br />
				Get started on <Link target="_blank" href="https://etke.cc?utm_source=matrixrooms.info&utm_medium=stats&utm_campaign=matrixrooms.info"><span className="text-etke">etke.cc</span></Link> now.
			</h3>
		</div>
	</div>);
};