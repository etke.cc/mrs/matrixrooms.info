import { Suspense } from 'react';
import SearchForm from '../../components/SearchForm';
import SearchComponent from "../../components/SearchComponent";

function SearchBarFallback() {
	return <>Loading...</>
}

export default function SearchPage() {
	return (<div>
		<Suspense fallback={<SearchBarFallback />}>
			<SearchForm />
		</Suspense>
		<Suspense fallback={<SearchBarFallback />}>
			<SearchComponent />
		</Suspense>
	</div>)
}
