import Link from 'next/link';
import Image from 'next/image';
import PlausibleProvider from 'next-plausible';
import './globals.css';
import donateImg from './donate.svg';
import Header from '../components/Header';

export const metadata = {
  title: 'Find rooms on the Matrix Network',
  description: 'Search for rooms on the Matrix Network',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <head>
        <meta name="referrer" content="origin" />
        <PlausibleProvider
          domain="matrixrooms.info"
          customDomain="https://uhoh.etke.cc"
          scriptProps={{
            src: 'https://uhoh.etke.cc/js/script.js',
          }}
          selfHosted
          enabled
        />
      </head>
      <body>
        <div className="flex flex-col justify-between items-center min-h-screen dark:bg-slate-900">
          <div className="mt-4 w-full 2xl:w-1/2">
            <Header />
            {children}
          </div>
          <footer className="bg-slate-800 text-white d bottom-0 left-0 w-full">
            <div className="flex flex-col xl:flex-row justify-between items-start md:items-center h-22 py-4 px-8">
              <Link className="text-gray-200 text-2xl mb-3" href="/">MatrixRooms.info<sup>α</sup></Link>
              <Link className="text-gray-200 mb-3" aria-current="page" href="/about">About</Link>
              <Link className="text-gray-200 mb-3" aria-current="page" href="/indexing">Indexing</Link>
              <Link className="text-gray-200 mb-3" aria-current="page" href="/deindexing">Deindexing</Link>
              <Link className="text-gray-200 mb-3" aria-current="page" href="/stats">Stats</Link>
              <Link target="_blank" className="text-gray-200 mb-3" aria-current="page" href="https://matrix.to/#/#mrs:etke.cc">Community Room</Link>
              <Link target="_blank" className="text-gray-200 mb-3" aria-current="page" href="https://liberapay.com/mrs/donate">
                <Image src={donateImg} alt="Donate"/>
              </Link>
              <Link target="_blank" className="text-gray-200 text-xl mb-3" href="https://etke.cc?utm_source=matrixrooms.info&utm_medium=footer&utm_campaign=matrixrooms.info">Built by <span className="text-etke">etke.cc</span> - managed Matrix servers hosting</Link>
            </div>
          </footer>
        </div>
      </body>
    </html>
  );
}
