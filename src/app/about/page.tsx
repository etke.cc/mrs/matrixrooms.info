export default function AboutPage() {
	return <div className="mb-5 dark:text-white dark:bg-slate-800 rounded-lg mt-5">
		<div className="max-w-full border-slate-100 p-4 text-gray-600 dark:text-white">
			<h2 className="block dark:text-white text-xl md:text-2xl mb-3 text-center font-bold">About MatrixRooms.info</h2>
			<section className="mb-5">
				<h2>Our Mission</h2>
				<p>
					Welcome to <a href="https://matrixrooms.info">MatrixRooms.info</a>,
					your gateway to the decentralized world of <a href="https://matrix.org" target="_blank">Matrix</a> chat rooms. <a href="https://matrixrooms.info">MatrixRooms.info</a> is a live demonstration instance of the <a href="https://gitlab.com/etke.cc/mrs/api" target="_blank">Matrix Rooms Search (MRS)</a>,
					a powerful open-source project developed and operated by <a href="https://etke.cc?utm_source=matrixrooms.info&utm_medium=about&utm_campaign=matrixrooms.info">etke.cc</a>.
					Our mission is to make it easier for you to explore and discover Matrix rooms
					across the vast Matrix federation.
				</p>
			</section>

			<section className="mb-5">
				<h2>Instance Rules</h2>
				<p>
					We take your safety and security seriously. Our instance operates with a strict set of rules to ensure a
					safe and welcoming environment for all users. Our policy for reporting rooms is straightforward: we will
					only remove rooms that violate the law, such as those involving child sexual abuse material (CSAM) or child
					pornography. We believe in free speech and open communication, but we draw the line at illegal activities.
				</p>
			</section>

			<section className="mb-5">
				<h2><a href="https://github.com/matrix-org/matrix-spec-proposals/pull/1929" target="_blank">MSC1929</a> Integration</h2>
				<p>
					Matrix Rooms Search is constantly evolving to provide you with the best possible experience. One of the
					latest additions is the integration of MSC1929, a Matrix Spec Change that enhances transparency and
					communication. With MSC1929, we can automatically forward reports for specific rooms to the administrators
					of the Matrix server to which the room belongs. This feature ensures that issues are seamlessly directed
					to the relevant administrators, making the Matrix ecosystem safer and more accountable.
				</p>
			</section>

				<section className="mb-5">
				<h2>Support Us</h2>
					<p>
					We rely on your support to continue our mission of improving and maintaining the Matrix Rooms Search. If
					you find our service valuable and wish to contribute, there are two main ways you can help:
					</p>
					<p>
						<strong>Donations:</strong> Your generous contributions help us cover the costs of running <a href="https://matrixrooms.info">MatrixRooms.info demo instance</a> and support ongoing development efforts of <a href="https://gitlab.com/etke.cc/mrs/api" target="_blank">Matrix Rooms Search project</a>. Please consider making a donation on our <strong><a href="https://liberapay.com/mrs/donate" target="_blank">Liberapay page</a></strong>. Every donation, no matter the size, is greatly appreciated.
						</p>
						<p>
						<strong>Contributions:</strong> If you're a developer or have technical expertise, you can actively
							contribute to the Matrix Rooms Search project by exploring our <strong><a href="https://gitlab.com/etke.cc/mrs/api" target="_blank">GitLab repository</a></strong>. Feel free to dive into the source
						code, submit bug reports, suggest enhancements, or even submit your own contributions to help us make
						MRS even better.
						</p>
				</section>

				<section className="mb-5">
				<h2>How to remove a server from index?</h2>
					<p>
						There are several options available:
						<ul>
							<li>1. <strong><a href="/deindexing/#federation" target="_blank">by unpublishing your directory</a></strong></li>
							<li>2. <i>(specific rooms only)</i> <strong><a href="/deindexing#room-topic" target="_blank">by adding string to a room's topic</a></strong></li>
							<li>3. <i>(deprecated)</i> <strong><a href="/deindexing#robots-txt" target="_blank">by robots.txt</a></strong></li>
							<li>4. <i>(discouraged)</i> by sending email to <code>matrixrooms</code> [at] <code>etke.host</code> from an email address listed in your server's MSC1929 support file. Why? Because that way we can validate your ownership of the server. Subject should contain server name you want to remove from index. Once we read the email, we will block your server on MatrixRooms.info in a week. Emails to that mailbox are checked manually, so it will take us some time. It would be way better, faster, and easier to use the options above.</li>
						</ul>
						Please, keep in mind that MatrixRooms.info is heavily cached (CDN), plus removal happens only during the daily reindex process, and thus your server's rooms will completely vanish from search results in a few days after the server was added to the blocklist.
					</p>
				</section>

				<section className="mb-5">
					<p>
						Thank you for trying <a href="https://matrixrooms.info">MatrixRooms.info</a> and for supporting our efforts to build <a href="https://gitlab.com/etke.cc/mrs/api" target="_blank">Matrix Rooms Search</a> and make Matrix a safer, more
					transparent, and more accessible platform for all. Together, we can create a thriving Matrix community where
					everyone can find their place and connect with others.
					</p>
				</section>
		</div>
		</div>
};
