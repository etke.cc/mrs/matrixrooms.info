import Link from 'next/link';
import { Suspense } from 'react';
import SearchForm from '../components/SearchForm';

const SearchQueryExample = ({ query }: { query: string }) => {
  return <Link href={`/search?q=${query}`} className="text-blue-500 dark:text-gray-200 inline-block m-3 text-xl cursor-pointer">{query}</Link>
};

function SearchBarFallback() {
  return <>placeholder</>
}

export default function Home(props: any) {
  return (
    <>
      <Suspense fallback={<SearchBarFallback />}>
        <SearchForm />
      </Suspense>
      <div className="block my-2 mx-auto w-1/2">
        <h1 className="text-2xl text-center my-8 dark:text-slate-400">Example search queries:</h1>
      </div>
      <div className="flex justify-between flex-wrap px-4 lg:px-8">
        <SearchQueryExample query="Matrix Hosting" />
        <SearchQueryExample query="FOSS" />
        <SearchQueryExample query="ChatGPT" />
        <SearchQueryExample query="mastodon" />
        <SearchQueryExample query="Android" />
        <SearchQueryExample query="food" />
        <SearchQueryExample query="linux" />
      </div>
    </>
  );
}
