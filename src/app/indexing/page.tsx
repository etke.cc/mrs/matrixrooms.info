export default function IndexingPage() {
	return <div className="mb-5 dark:text-white dark:bg-slate-800 rounded-lg mt-5">
		<div className="max-w-full border-slate-100 p-4 text-gray-600 dark:text-white">
			<h2 className="block dark:text-white text-xl md:text-2xl mb-3 text-center font-bold">How to add your server's public rooms to the index</h2>

			<section className="mb-5">
				<p>Use POST `/discover/server_name` endpoint, here is example using an `example.com` homeserver:</p>
				<pre className="bg-gray-100 dark:bg-slate-900 rounded-md p-2 overflow-auto"><code>curl -X POST https://apicdn.matrixrooms.info/discover/example.com</code></pre>
				<p>If your server publishes room directory over federation and has public rooms within the directory, they will appear in the search index after the next full reindex process (should be run daily).</p>
				<p>Please keep in mind that to have a room indexed, you have to:</p>
				<ol className="list-decimal ml-6 mb-3">
					<li>Explicitly mark a room federatable when creating it</li>
					<li>Explicitly mark a room as public in room settings</li>
					<li>Explicitly publish a room in the room directory</li>
					<li>Explicitly publish your room catalog over federation</li>
				</ol>
			</section>

			<section className="mb-5">
				<h3 className="dark:text-white text-lg md:text-xl mb-3 font-bold">Why my server and its public rooms aren't discovered/parsed/included?</h3>
				<p>Your server must publish public rooms over federation (`/_matrix/federation/v1/publicRooms` endpoint), eg: <code>https://matrix.etke.cc:8448/_matrix/federation/v1/publicRooms</code></p>
				<p><strong>I get error on public rooms endpoint</strong>, something like:</p>
				<pre className="bg-gray-100 dark:bg-slate-900 rounded-md p-2 overflow-auto"><code>"errcode":"M_FORBIDDEN","error":"You are not allowed to view the public rooms list of example.com"</code></pre>
				<p>In that case you should adjust your server's configuration. For synapse, you need to add the following config options in the <code>homeserver.yaml</code>:</p>
				<pre className="bg-gray-100 dark:bg-slate-900 rounded-md p-2 overflow-auto"><code>allow_public_rooms_over_federation: true</code></pre>
				<p>in case of <a href="https://gitlab.com/etke.cc/ansible" className="text-blue-500">etke.cc/ansible</a> and <a href="https://github.com/spantaleev/matrix-docker-ansible-deploy" className="text-blue-500">mdad</a>, add the following to your vars.yml:</p>
				<pre className="bg-gray-100 dark:bg-slate-900 rounded-md p-2 overflow-auto"><code>matrix_synapse_allow_public_rooms_over_federation: true</code></pre>
			</section>
		</div>
	</div>
};
