export default function DeindexingPage() {
	return <div className="mb-5 dark:text-white dark:bg-slate-800 rounded-lg mt-5">
		<div className="max-w-full border-slate-100 p-4 text-gray-600 dark:text-white">
			<h2 className="block dark:text-white text-xl md:text-2xl mb-3 text-center font-bold">How to deindex/unlist/unpublish/block/remove your server's rooms from index</h2>

			<section className="mb-5">
				<h3 className="dark:text-white text-lg md:text-xl mb-3 font-bold">If your server publishes room directory over federation and has public rooms within the directory,</h3>
				<p>they will appear in the search index after the next full reindex process (should be run daily).</p>
				<p>Please keep in mind that to have a room indexed, you have to:</p>
				<ol className="list-decimal ml-6 mb-3">
					<li>Explicitly mark a room federatable when creating it</li>
					<li>Explicitly mark a room as public in room settings</li>
					<li>Explicitly publish a room in the room directory</li>
					<li>Explicitly publish your room catalog over federation</li>
				</ol>
				<p>When you do all of those steps, you clearly understand the consequences of your decisions, i.e. a particular room that was made public, federatable, published in room catalog and then shared room catalog over federation will be accessed over federation.</p>
				<p>MRS can't index a room if you didn't explicitly allow a room to be visible over federation. You either publish something over federation, or not. MRS is not a special thing, it uses the same API and the same set of rules as any other matrix server does.</p>
			</section>

			<section id="federation" className="mb-5">
				<h3 className="dark:text-white text-lg md:text-xl mb-3 font-bold">unpublish your room directory from the federation</h3>
				<p>If your server is indexed, that means you explicitly published your public rooms directory over federation. If you don't like that the information you explicitly published over federation is accessed over federation, you should consider unpublishing it.</p>
				<p>For synapse, you need to add the following config options in the <code>homeserver.yaml</code>:</p>
				<pre className="bg-gray-100 dark:bg-slate-900 rounded-md p-2 overflow-auto"><code>allow_public_rooms_over_federation: false</code></pre>
				<p>in case of <a href="https://gitlab.com/etke.cc/ansible" className="text-blue-500">etke.cc/ansible</a> and <a href="https://github.com/spantaleev/matrix-docker-ansible-deploy" className="text-blue-500">mdad</a>, add the following to your vars.yml:</p>
				<pre className="bg-gray-100 dark:bg-slate-900 rounded-md p-2 overflow-auto"><code>matrix_synapse_allow_public_rooms_over_federation: false</code></pre>
				<p>However, if you think that "publishing over federation, but not for that particular member of the federation" segregation is a good thing for Matrix protocol, MRS has several options to unlist/unpublish/block/remove your server and its rooms from indexing.</p>
			</section>

			<section id="room-topic" className="mb-5">
				<h3 className="dark:text-white text-lg md:text-xl mb-3 font-bold">(specific room) include special string in room topic</h3>
				<p>You could add the following string into your room topic/description to prevent it from indexing (exact match):</p>
				<blockquote className="bg-gray-100 dark:bg-slate-900 border-l-4 border-blue-500 p-4">
					<p>matrixrooms.info: noindex</p>
				</blockquote>
			</section>

			<section id="robots-txt" className="mb-5">
				<h3 className="dark:text-white text-lg md:text-xl mb-3 font-bold">(deprecated) robots.txt</h3>
				<p>While robots.txt is not part of the Matrix protocol, that's the easiest and convenient way to prevent your server from indexing. MRS supports several made-up endpoints to prevent indexing your server.</p>
				<blockquote className="bg-gray-100 dark:bg-slate-900 border-l-4 border-blue-500 p-4">
					<p><strong>NOTE:</strong> due to the fact robots.txt is not part of the Matrix protocol, MRS treats it a bit differently than usual bots. <code>User-agent: MRSBot</code> <strong>MUST</strong> be present in your robots.txt file to make it work (<code>User-agent: *</code> will be ignored!)</p>
				</blockquote>

				<section className="mb-5">
					<h4 className="dark:text-white text-base md:text-lg mb-3 font-bold">whole server</h4>
					<p>Just add the following to your <code>robots.txt</code>:</p>
					<pre className="bg-gray-100 dark:bg-slate-900 rounded-md p-2 overflow-auto"><code>User-agent: MRSBot<br />Disallow: /_matrix/federation/v1/publicRooms</code></pre>
				</section>

				<section>
					<h4 className="dark:text-white text-base md:text-lg mb-3 font-bold">specific rooms</h4>
					<p>Just add the following to your <code>robots.txt</code> (you can add multiple <code>Disallow</code> lines, one per room ID):</p>
					<pre className="bg-gray-100 dark:bg-slate-900 rounded-md p-2 overflow-auto"><code>User-agent: MRSBot<br />Disallow: /_matrix/federation/v1/publicRooms/!yourRoomID:example.com</code></pre>
				</section>
			</section>

			<section>
				<h3 className="dark:text-white text-lg md:text-xl mb-3 font-bold">contact instance maintainers</h3>
				<p>Contacts listed on the <a href="/about">About</a> page</p>
			</section>
		</div>
	</div>
};
