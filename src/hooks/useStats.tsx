import { getBaseUrl } from "@/utils";
import { useEffect, useState } from "react";

type SERVER_STATS_DETAILS = {
	online: number;
	indexable: number;
	parsed: number;
};

type SERVER_STATS_DATA = {
	date: string,
	details: {
		rooms: SERVER_STATS_DETAILS,
		servers: SERVER_STATS_DETAILS
	}
};

export type BASE_CHART_DATA = {
	name: string;
	date: string;
};

export type ROOMS_CHART_DATA = {
	parsed: number;
} & BASE_CHART_DATA;

export type SERVERS_CHART_DATA = {
	online: number;
	indexable: number;
} & BASE_CHART_DATA;

export type STATS_DATA = {
	rooms: ROOMS_CHART_DATA[],
	servers: SERVERS_CHART_DATA[]
};

export default function useStats() {
	const SERVERS_DATA_NAME = "Indexed Servers";
	const ROOMS_DATA_NAME = "Indexed Rooms";
	const [ isLoading, setIsLoading ] = useState(true);
	const [ statsData, setStatsData ] = useState<STATS_DATA>();

	const fetchStats = async () => {
		const baseUrl = getBaseUrl();
		const response = await fetch(`${baseUrl}/stats`);
		const serversData: SERVERS_CHART_DATA[] = [];
		const roomsData: ROOMS_CHART_DATA[] = [];

		if (!response.ok) {
			return;
		}
		if (response.status !== 200) {
			return;
		}
		const data = await response.json();
		if (data && !data.timeline) {
			return;
		}
		data.timeline.forEach((dataSnapshot: SERVER_STATS_DATA) => {
			roomsData.push({
				name: ROOMS_DATA_NAME,
				parsed: dataSnapshot.details.rooms.parsed,
				date: dataSnapshot.date,
			});
			serversData.push({
				name: SERVERS_DATA_NAME,
				indexable: dataSnapshot.details.servers.indexable,
				online: dataSnapshot.details.servers.online,
				date: dataSnapshot.date,
			});
		});
		setStatsData({
			rooms: roomsData,
			servers: serversData,
		});
		setIsLoading(false);
	}

	useEffect(() => {
		fetchStats();
	}, []);

	return {
		isLoading,
		statsData
	};
};