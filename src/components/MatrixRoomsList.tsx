
import { MatrixRoomEntry } from "./MatrixRoomEntry";

export interface MatrixRoom {
	id: string;
	name: string;
	alias: string;
	topic: string;
	members: number;
	server: string;
	avatar_url: string;
	language: string;
}

interface MatrixRoomsListProps {
	rooms: MatrixRoom[];
	hasSearched: boolean;
	hasMore: boolean;
	loadMore: () => void;
	isFetching: boolean;
}

export default function MatrixRoomsList(props: MatrixRoomsListProps) {
	const rooms = props.rooms;
	if (rooms.length === 0 && props.hasSearched) {
		return <div className="block text-center text-xl container p-4 dark:text-white">No rooms found for search criteria.</div>;
	}

	const handleLoadMore = () => {
		if (props.isFetching) {
			return;
		}

		props.loadMore();
	}

	return <div className="mb-5 dark:text-white dark:bg-slate-800 rounded-lg mt-5">
		<div className="border-b border-gray-300 dark:border-slate-500 bg-yellow-50">
			<div className="max-w-full border-slate-100 p-4 text-gray-600">
				<div className="flex items-center">
					<span className="mr-2 self-start py-2 text-2xl text-center align-middle font-mono" style={{ width: 40, height: 40 }}>
						ℹ️
					</span>
					<span>
						<span className="font-bold">Browse & search matrixrooms.info right from your Element app?</span>
						<br />
						Yes, you can. See <a className="text-blue-400 break-words inline-block font-bold" href="https://gitlab.com/etke.cc/mrs/api/-/blob/main/docs/integrations.md" rel="noopener" target="_blank">this guide!</a>
					</span>
				</div>
			</div>
		</div>
		{rooms.map((room) => {
			return <MatrixRoomEntry room={room} />
		})}
		{props.hasMore && <button className="bg-etke text-white font-bold p-3 rounded-md block w-40 mx-auto text-center my-5" onClick={handleLoadMore}>
			{props.isFetching ? "Loading..." : "Load more"}
		</button>}
	</div>
};
