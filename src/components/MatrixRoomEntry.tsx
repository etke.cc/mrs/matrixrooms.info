import React from "react";
import { ReactElement } from "react";
import { MatrixRoom } from "./MatrixRoomsList";
import Image from 'next/image';
import ReportRoom from "./ReportRoom";

const renderMembers = (members: number): ReactElement => {
	return (
		<span
			className="inline-block bg-slate-200 px-2 py-1 text-sm text-center align-baseline rounded-full font-semibold"
			title={members + ' room ' + (members === 1 ? 'member' : 'members')}
		>
			{members === 1 ? '👤' : '👥'}
			&nbsp;
			{members}
		</span>
	);
}

const roomAliasURL = (roomAlias: string): string => {
	return `https://matrix.to/#/${roomAlias}`;
}
const roomIDURL = (roomID: string, roomServer: string): string => {
	return `https://matrix.to/#/${roomID}?via=${roomServer}`;
}

interface MatrixRoomEntryProps {
	room: MatrixRoom;
}

export const MatrixRoomEntry: React.FC<MatrixRoomEntryProps> = ({ room }) => {
	let roomJoinName: string = room.alias ? room.alias : room.id;
	let roomURL: string = room.alias ? roomAliasURL(room.alias) : roomIDURL(room.id, room.server);

	return (
		<div key={room.id + '-name'} className="border-b border-gray-300 dark:border-slate-500">
			<div className="max-w-full border-slate-100 p-4 text-gray-600 dark:text-white">
				<a href={roomURL} rel="noopener" target="_blank" className="inline-block mb-4 font-bold">
					<span className="flex flex-column items-center">
						<span className="mr-2 self-start py-2">
							{room.avatar_url
								? <div className="inline-block mr-1 align-middle rounded-lg" style={{ width: "40px", height: "40px" }}>
									<Image
										width={40}
										height={40}
										src={room.avatar_url}
										alt={room.name + " avatar"}
									/>
								</div>
								: <div className="inline-block mr-1 align-middle bg-gray-300 rounded-lg" style={{ width: "40px", height: "40px" }}></div>
							}
						</span>

						<span className="break-all">
							{room.name}
							<br />
							<span className="text-blue-400 text-sm break-words inline-block">{roomJoinName}</span>
						</span>
					</span>
				</a>

				<div className="flex flex-row mb-2 justify-between">
					<div>
						<div className="inline-block mr-2 dark:text-slate-800">
							{renderMembers(room.members)}
						</div>
						<div className={"inline-block mr-2 dark:text-slate-800" + (room.language === '-' ? " hidden" : "")}>
							<span
								className="inline-block bg-slate-200 px-2 py-1 text-sm text-center align-baseline rounded-full font-semibold"
								title={'Language detected as: ' + (room.language === '-' ? 'Unknown' : room.language)}
							>
								{room.language}
							</span>
						</div>
					</div>
					<div>
						<ReportRoom room={room} />
					</div>
				</div>

				<div className="my-1 break-words">
					{room.topic}
				</div>
			</div>
		</div>
	);
};