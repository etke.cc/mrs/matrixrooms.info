'use client';
import useStats from '@/hooks/useStats';
import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer, Legend } from 'recharts';

export default function TimeLineStats() {
	const { isLoading, statsData } = useStats();

	if (isLoading) {
		return <div className="mt-4"><h1 className="text-2xl">Loading Matrix Rooms and Servers data...</h1></div>
	}

	if (!statsData) {
		return <div>Error..</div>;
	}

	return (<div>
		<h2 className="mb-4 text-3xl dark:text-white">Matrix Rooms:</h2>
		<div className="my-4">
			<ResponsiveContainer width="100%" height={480}>
				<AreaChart
					margin={{ top: 55, right: 0, left: 10, bottom: 5  }}
					style={{overflow: "visible"}}
					data={statsData.rooms}
				>
					<CartesianGrid strokeDasharray="3 3" />
					<XAxis dataKey="date" />
					<YAxis dataKey={"parsed"} />
					<Tooltip />
					<Legend height={66} iconSize={40} />
					<Area name="Parsed" type="monotone" dataKey={"parsed"} stroke="#8884d8" fill="#8884d8" />
				</AreaChart>
			</ResponsiveContainer>
		</div>
		<h2 className="mt-4 text-3xl dark:text-white">Servers:</h2>
		<div className="mt-4">
			<ResponsiveContainer width="100%" height={480}>
				<AreaChart
					data={statsData.servers}
				>
					<defs>
						<linearGradient id="indexable" x1="0" y1="0" x2="0" y2="1">
							<stop offset="5%" stopColor="#f39200" stopOpacity={0.8}/>
							<stop offset="95%" stopColor="#f39200" stopOpacity={0}/>
						</linearGradient>
						<linearGradient id="online" x1="0" y1="0" x2="0" y2="1">
							<stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8}/>
							<stop offset="95%" stopColor="#82ca9d" stopOpacity={0}/>
						</linearGradient>
					</defs>
					<CartesianGrid strokeDasharray="3 3" />
					<XAxis dataKey="date" />
					<YAxis />
					<Tooltip />
					<Legend height={66} iconSize={40} />
					<Area name="Indexable" type="monotone" dataKey="indexable" stroke="#f39200" fillOpacity={1} fill="url(#indexable)" />
					<Area name="Online"type="monotone" dataKey="online" stroke="#82ca9d" fillOpacity={1} fill="url(#online)" />
				</AreaChart>
			</ResponsiveContainer>
		</div>
	</div>)
}