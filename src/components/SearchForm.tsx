'use client';

import Link from 'next/link';
import { useRouter, useSearchParams } from 'next/navigation';
import { FormEvent, useEffect, useState } from 'react';

export default function SearchForm() {
	const searchParams = useSearchParams();
	const [searchQuery, setSearchQuery] = useState(searchParams.get('q') || '');
	const router = useRouter();

	useEffect(() => {
		setSearchQuery(searchParams.get('q') || '');
	}, [searchParams.get('q')]);

	const submitForm = async (event: FormEvent) => {
		event.preventDefault();
		if (searchQuery) {
			router.push('/search?q=' + encodeURIComponent(searchQuery));
		}
	};

	return <div className="bg-zinc-50 dark:bg-slate-800 p-4 md:p-8 w-full rounded-lg mt-5">
		<form onSubmit={submitForm} className="w-full">
			<div className="flex flex-row items-center">
				<input
					onChange={(event) => setSearchQuery(event.target.value)}
					value={searchQuery}
					className="text-2xl w-full bg-white dark:bg-slate-500 dark:text-white border border-slate-300 rounded-l-md border-r-0 p-2 focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 placeholder:italic placeholder:text-slate-400"
					type="text" />
				<button className="border text-4xl p-1 rounded-r-md border-l-0">🔍</button>
			</div>
		</form>
	</div>;
}
