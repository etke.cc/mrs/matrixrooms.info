'use client';

import { useEffect, useState } from "react";
import { getBaseUrl } from "@/utils";
import Link from 'next/link';
import Image from 'next/image';

export interface Stats {
	rooms: number;
	servers: number;
}

const API_BASE_URL = getBaseUrl();

export default function Header() {
	const [stats, setStats] = useState<Stats>({servers: 0, rooms: 0});
	useEffect(() => {
		const getData = async () => {
			try {
				const response = await fetch(`${API_BASE_URL}/stats`);
				if (!response.ok) {
					return;
				}
				if (response.status !== 200) {
					return;
				}
				const data = await response.json();
				if (data) {
					setStats(data);
				}
			} catch (e) {
				return;
			}
		};

		getData();
	}, []);

	return (<div className="bg-zinc-50 dark:bg-slate-800 p-4 md:p-8 w-full rounded-lg">
		<Link href="/" className="block dark:text-white text-2xl md:text-4xl mb-3 text-center font-bold">
			<div className="inline-block mr-1 align-middle rounded-lg" style={{width: "40px", height: "40px"}}>
				<Image src="/logo.webp" width={40} height={40} alt="MatrixRooms.info - an MRS instance"/>
			</div>
			MatrixRooms.info<sup>α</sup>
		</Link>
		<h2 className="text-lg dark:text-slate-400 mb-5 text-center">
			Search among {stats.rooms ? stats.rooms : '250k+'} Matrix rooms on {stats.servers ? stats.servers : '7k+'} servers
		</h2>
	</div>)
}
