import React, { useState, useRef, useEffect } from "react";
import { MatrixRoom } from "./MatrixRoomsList";

export default function ReportRoom({ room }: {room: MatrixRoom}) {
	const [showReport, setShowReport] = useState<boolean>(false);
	const [isReported, setIsReported] = useState<boolean>(false);
	const [reportReason, setReportReason] = useState<string>("");

	const reportRef = useRef<HTMLFormElement>(null);

	useEffect(() => {
		function handleClickOutside(event: any): void {
			if (reportRef.current && !reportRef.current.contains(event.target)) {
				if (showReport) {
					setShowReport(false);
				}
			}
		}

		document.addEventListener("mousedown", handleClickOutside);

		return () => {
			document.removeEventListener("mousedown", handleClickOutside);
		};
	}, [showReport, reportRef]);

	const toggleReport = () => {
		setShowReport(!showReport);
	};

	const reportRoom = async () => {
		if (reportReason === "") {
			alert("Please enter report reason.");
			return;
		}

		const response = await fetch(`https://apicdn.matrixrooms.info/mod/report/${room.id}`, {
			"method": "POST",
			"headers": {
				"Content-Type": "application/json",
			},
			"body": JSON.stringify({"reason": reportReason})
		})

		if (response.status === 202) {
			setIsReported(true);
		}
	};

	if (isReported) {
		return <div>Room Reported</div>;
	}

	return (<form onSubmit={(ev) => { ev.preventDefault(); reportRoom() }} ref={reportRef}>
			<div onClick={() => { toggleReport() }} className="pr-4 block cursor-pointer" title="Report room">⚠️</div>
			{showReport && <div className="relative">
				<div className="absolute top-0 right-0 flex flex-row bg-gray-200  dark:bg-slate-500 p-2 rounded-lg h-14 md:h-12">
					<input
						enterKeyHint="send"
						autoFocus
						className="border-2
							border-r-0
							rounded-l-lg
							px-1
							placeholder:italic
							placeholder:text-slate-400
							dark:bg-slate-800
							border-white
							focus:ring-0
							focus:outline-none"
						placeholder="Reason for reporting"
						type="text"
						name="reason"
						value={reportReason}
						onKeyDown={(ev: React.KeyboardEvent<HTMLInputElement>) => {
							if (ev.key === "Enter") {
								ev.preventDefault();
								reportRoom();
								return;
							}
						}}
						onChange={(ev: React.ChangeEvent<HTMLInputElement>) => {
							setReportReason(ev.target.value)
						}}
					/>
					<button type="submit" className="border border-l-0 rounded-r-lg border-white text-slate-800 block text-xl leading-4 px-2">➡️</button>
				</div>
			</div>}
	</form>);
};
