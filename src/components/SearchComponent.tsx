'use client';

import { usePlausible } from 'next-plausible';
import { useEffect, useState } from "react";
import MatrixRoomsList, { MatrixRoom } from "../components/MatrixRoomsList";
import { useSearchParams } from "next/navigation";
import { getBaseUrl } from "@/utils";

const LIMIT = 30;

export default function SearchComponent() {
	const API_BASE_URL = getBaseUrl();
	const [rooms, setRooms] = useState<MatrixRoom[]>([]);
	const [isFetching, setIsFetching] = useState(false);
	const [hasSearched, setHasSearched] = useState(false);
	const [offset, setOffset] = useState<number>(0);
	const [hasMore, setHasMore] = useState<boolean>(false);

	const plausible = usePlausible();
	const searchParams = useSearchParams();
	const searchQuery = searchParams.get('q') || '';

	const getApiURL = (queryString: string, limit: number, offset: number): string => {
		return `${API_BASE_URL}/search/${encodeURIComponent(queryString)}/${limit}/${offset}`;
	};

	useEffect(() => {
		if (offset == 0) {
			plausible('Search', { props: { query: searchQuery } });
		}
		setRooms([]);
		setIsFetching(false);
		setOffset(0);
		setHasSearched(false);
		setHasMore(false);
	}, [searchQuery]);

	useEffect(() => {
		let isCancelled = false;

		const getData = async () => {
			setHasSearched(true);
			setIsFetching(true);
			setHasMore(false);

			try {
				const response = await fetch(getApiURL(searchQuery, LIMIT + 1, offset));
				if (!response.ok) {
					setIsFetching(false);
					return;
				}

				if (response.status !== 200) {
					setIsFetching(false);
					return;
				}

				const data = await response.json();
				if (!isCancelled && data) {
					let newRooms = data;
					if (newRooms.length > LIMIT) {
						setHasMore(true);
						newRooms.pop();
					}
					setRooms((rooms) => {
						return [...rooms, ...newRooms];
					});

				}
				setIsFetching(false);
			} catch (e) {
				setIsFetching(false);
				return;
			}
		};

		getData();

		return () => {
			isCancelled = true;
		};
	}, [searchQuery, offset]);

	const loadMore = () => {
		setOffset((offset) => {
			return offset + LIMIT;
		});
	};

	return (<div>
		{isFetching && rooms.length === 0
			? <div className="block my-8 text-center dark:text-slate-400"><h3 className="text-2xl">Loading...</h3></div>
			: <MatrixRoomsList hasSearched={hasSearched} isFetching={isFetching} rooms={rooms} hasMore={hasMore} loadMore={loadMore} />
		}
	</div>)
}
