/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    appDir: true,
  },
  output: "export",
  images: { unoptimized: true },
  trailingSlash: true,
  distDir: 'dist',
}

module.exports = nextConfig
