# Shows help
default:
    @just --list --justfile {{ justfile() }}

run:
    npm run dev

build: _dist_dir _node_modules
    npm run build

_node_modules:
    npm install

_dist_dir:
    #!/bin/sh
    if ! [ -d dist ]; then
        mkdir dist
    fi

# Serves the latest build (from dist) on a given port
serve port="3000": _dist_dir
    /usr/bin/env docker run \
    -it \
    --rm \
    -p 127.0.0.1:{{ port }}:80 \
    --mount type=bind,src={{ justfile_directory() }}/dist,dst=/usr/share/nginx/html,ro \
    docker.io/nginx:1.23.4-alpine

deploy:
    @echo "uploading website..."
    @bunny-upload -c ${BUNNY_CONFIG}
    @echo "done"
